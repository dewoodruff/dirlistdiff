#!/bin/bash

# 4/6/12 - Dan Woodruff www.makethenmakeinstall.com
# This is a simple script to compare the current listing of a directory
# with a previously known listing of the same directory. 
# If there is a change, it will mail the changes to a specified email address.

# validate the location selected is a directory
[ -d "$1" ] || { echo "Usage: $0 directory"; exit 0; }
fullpath=`readlink -f $1`

##### Configuration options
# email message recipients
recipients="e@mail.com"
# email message subject
subject="Directory contents have changed. dirlistdiff for $fullpath"
# directory to store db files, without trailing slash. default is the script's dir
dbdir=$(dirname $(readlink -f $0))
# command to run that creates and stores the database
lscmd="ls"
# to run the script without writing to stdout, as for a cron job, set to 1
silent=1
#####

# sanitize the directory name for use in the db file name
cleaned_dir=$(echo "$1" | sed 's/\/$//' | sed 's/\//_/g')
dbfile=$dbdir"/dirlistdiff-$cleaned_dir.txt"

# if the file doesn't yet exist, create one and exit
if [ ! -f "$dbfile" ]; then
	[ $silent -eq 0 ] && echo "First run, creating $dbfile."
	$($lscmd $1 > $dbfile)
	exit 0
fi

# compare the current durectory listing with the contents of the database file
new=$($lscmd $1 | diff --suppress-common-lines $dbfile - | grep ">" | sed 's/> //')
removed=$($lscmd $1 | diff --suppress-common-lines $dbfile - | grep "<" | sed 's/< //')

mailstring=""
# build the string of changes to mail
if [ -n "$new" ]; then
	mailstring="$mailstring""New items:\n"
	mailstring="$mailstring$new\n\n"
fi
if [ -n "$removed" ]; then
	mailstring="$mailstring""Removed items:\n"
	mailstring="$mailstring$removed\n\n"
fi
# if anything changed, mail and update database
if [ -n "$mailstring" ]; then
	[ $silent -eq 0 ] && echo -e "$mailstring"
	echo -e "$mailstring" | mail -s "$subject" "$recipients"
	$($lscmd $1 > $dbfile)
fi
